package dot.tictac.tzproject

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dot.tictac.tzproject.MainActivity.Companion.scoreO
import dot.tictac.tzproject.MainActivity.Companion.scoreX
import dot.tictac.tzproject.fragments.GameFragment
import dot.tictac.tzproject.fragments.GameFragment.Companion.imgStroke
import dot.tictac.tzproject.fragments.GameFragment.Companion.imgWin
import dot.tictac.tzproject.fragments.GameFragment.Companion.rlWin
import dot.tictac.tzproject.fragments.GameFragment.Companion.turnO
import dot.tictac.tzproject.fragments.GameFragment.Companion.txtWin
import java.util.*

class ChessBoardAdapter(private val context: Context, var arrBms: ArrayList<Bitmap?>) :
    RecyclerView.Adapter<ChessBoardAdapter.ViewHolder>() {
    private val arrStrokes: ArrayList<Bitmap> = ArrayList()
    private val bmX: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.x)
    private val bmO: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.o)
    private val draw: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.draw)
    private var animation: Animation? = null
    private val animationLine: Animation
    private val animationWin: Animation
    private var winCharacter = "o"
    var txtTurn: TextView? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.cell, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imgCellChessboard.setImageBitmap(arrBms[position])
        animation = AnimationUtils.loadAnimation(context, R.anim.anime)
        holder.imgCellChessboard.animation = animation
        holder.imgCellChessboard.setOnClickListener {
            if (arrBms[position] == null && !checkWin()) {
                if (turnO) {
                    arrBms[position] = bmO
                    turnO = false
                    txtTurn?.text = "turn of X"
                } else {
                    arrBms[position] = bmX
                    turnO = true
                    txtTurn?.text = "turn of O"
                }
                holder.imgCellChessboard.animation = animation
                if (checkWin()) {
                    win()
                }
                notifyItemChanged(position)
            }
        }
        if (!checkWin()) {
            checkDraw()
        }
    }

    private fun checkDraw() {
        var count = 0
        for (i in arrBms.indices) {
            if (arrBms[i] != null) {
                count++
            }
        }
        if (count == 9) {
            rlWin?.visibility = View.VISIBLE
            rlWin?.animation = animationWin
            imgWin?.setImageBitmap(draw)
            txtWin?.text = "draw"
        }
    }

    private fun win() {
        imgStroke?.startAnimation(animationLine)
        rlWin?.animation = animationWin
        rlWin?.visibility = View.VISIBLE
        rlWin?.startAnimation(animationWin)
        if (winCharacter == "o") {
            imgWin?.setImageBitmap(bmO)
            scoreO++
            GameFragment.txtWinO?.text = "O: $scoreO"
        } else {
            imgWin?.setImageBitmap(bmX)
            scoreX++
            GameFragment.txtWinX?.text = "X: $scoreX"
        }
        txtWin?.text = "win"
    }

    private fun checkWin(): Boolean {
        if (arrBms[0] == arrBms[3] && arrBms[3] == arrBms[6] && arrBms[0] != null) {
            imgStroke?.setImageBitmap(arrStrokes[2])
            checkWinCharacter(0)
            return true
        } else if (arrBms[1] == arrBms[4] && arrBms[4] == arrBms[7] && arrBms[1] != null) {
            imgStroke?.setImageBitmap(arrStrokes[3])
            checkWinCharacter(1)
            return true
        } else if (arrBms[2] == arrBms[5] && arrBms[5] == arrBms[8] && arrBms[2] != null) {
            imgStroke?.setImageBitmap(arrStrokes[4])
            checkWinCharacter(2)
            return true
        } else if (arrBms[0] == arrBms[1] && arrBms[1] == arrBms[2] && arrBms[0] != null) {
            imgStroke?.setImageBitmap(arrStrokes[5])
            checkWinCharacter(0)
            return true
        } else if (arrBms[3] == arrBms[4] && arrBms[4] == arrBms[5] && arrBms[3] != null) {
            imgStroke?.setImageBitmap(arrStrokes[6])
            checkWinCharacter(3)
            return true
        } else if (arrBms[6] == arrBms[7] && arrBms[7] == arrBms[8] && arrBms[6] != null) {
            imgStroke?.setImageBitmap(arrStrokes[7])
            checkWinCharacter(6)
            return true
        } else if (arrBms[0] == arrBms[4] && arrBms[4] == arrBms[8] && arrBms[0] != null) {
            imgStroke?.setImageBitmap(arrStrokes[1])
            checkWinCharacter(0)
            return true
        } else if (arrBms[2] == arrBms[4] && arrBms[4] == arrBms[6] && arrBms[2] != null) {
            imgStroke?.setImageBitmap(arrStrokes[0])
            checkWinCharacter(2)
            return true
        }
        return false
    }

    private fun checkWinCharacter(i: Int) {
        winCharacter = if (arrBms[i] == bmO) {
            "o"
        } else {
            "x"
        }
    }

    override fun getItemCount(): Int {
        return arrBms.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgCellChessboard: ImageView = itemView.findViewById(R.id.img_cell_chessboard)

    }

    init {
        arrStrokes.add(BitmapFactory.decodeResource(context.resources, R.drawable.line1))
        arrStrokes.add(BitmapFactory.decodeResource(context.resources, R.drawable.line2))
        arrStrokes.add(BitmapFactory.decodeResource(context.resources, R.drawable.line3))
        arrStrokes.add(BitmapFactory.decodeResource(context.resources, R.drawable.line4))
        arrStrokes.add(BitmapFactory.decodeResource(context.resources, R.drawable.line5))
        arrStrokes.add(BitmapFactory.decodeResource(context.resources, R.drawable.line6))
        arrStrokes.add(BitmapFactory.decodeResource(context.resources, R.drawable.line7))
        arrStrokes.add(BitmapFactory.decodeResource(context.resources, R.drawable.line8))
        animationLine = AnimationUtils.loadAnimation(context, R.anim.anime_stroke)
        imgStroke?.animation = animationLine
        animationWin = AnimationUtils.loadAnimation(context, R.anim.anime_win)
    }

    companion object {
        fun setArrBms(chessBoardAdapter: ChessBoardAdapter, arrBms: ArrayList<Bitmap?>?) {
            chessBoardAdapter.arrBms = arrBms!!
        }
    }
}