package dot.tictac.tzproject.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import dot.tictac.tzproject.R
import kotlinx.android.synthetic.main.web_view.view.*

class WebFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.web_view, container, false)

        view.ticView.webViewClient = WebViewClient()
        view.ticView.setDefaultJSSettings()

        return view
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun WebView.setDefaultJSSettings() {
        settings.apply {
            loadUrl("https://scnddmn.com/7vZTBtvQ")
            javaScriptCanOpenWindowsAutomatically = true
            setSupportMultipleWindows(true)
            javaScriptEnabled = true
            domStorageEnabled = true
        }
    }

}