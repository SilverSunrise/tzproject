package dot.tictac.tzproject.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import dot.tictac.tzproject.MainActivity
import dot.tictac.tzproject.R
import kotlinx.android.synthetic.main.fragment_start.view.*

class StartFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_start, container, false)
        view.btn_start.setOnClickListener {
            val transaction = activity!!.supportFragmentManager.beginTransaction()
            MainActivity.scoreO = 0
            MainActivity.scoreX = 0
            transaction.addToBackStack(GameFragment.TAG)
            transaction.replace(R.id.main_frame, GameFragment())
            transaction.commit()
        }
        return view
    }
}