package dot.tictac.tzproject.fragments

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dot.tictac.tzproject.ChessBoardAdapter
import dot.tictac.tzproject.MainActivity
import dot.tictac.tzproject.R
import kotlinx.android.synthetic.main.fragment_game.view.*
import java.util.ArrayList

class GameFragment : Fragment() {
    private var rvChessboard: RecyclerView? = null
    private var chessBoardAdapter: ChessBoardAdapter? = null
    private var btnReset: Button? = null
    private var btnAgain: Button? = null
    private var btnHome: Button? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_game, container, false)
        rvChessboard = view.findViewById(R.id.rv_chessboard)
        txtTurn = view.findViewById(R.id.txt_turn)
        btnReset = view.findViewById(R.id.btn_reset)
        imgStroke = view.findViewById(R.id.img_stroke)
        rlWin = view.findViewById(R.id.rl_win)
        txtWinX = view.findViewById(R.id.txt_win_x)
        txtWinO = view.findViewById(R.id.txt_win_o)
        txtWin = view.findViewById(R.id.txt_win)
        imgWin = view.findViewById(R.id.img_win)
        btnAgain = view.findViewById(R.id.btn_again)
        btnHome = view.findViewById(R.id.btn_home)
        val arrBms = ArrayList<Bitmap?>()
        for (i in 0..8) {
            arrBms.add(null)
        }
        chessBoardAdapter = ChessBoardAdapter(context!!, arrBms)
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, 3)
        view.rv_chessboard.layoutManager = layoutManager
        view.rv_chessboard.adapter = chessBoardAdapter
        view.btn_reset.setOnClickListener { reset() }
        view.btn_again.setOnClickListener {
            view.rl_win.visibility = View.INVISIBLE
            reset()
        }
        view.btn_home.setOnClickListener {
            reset()
            fragmentManager!!.popBackStack()
        }
        return view
    }

    private fun reset() {
        val arrBms = ArrayList<Bitmap?>()
        for (i in 0..8) {
            arrBms.add(null)
        }
        ChessBoardAdapter.setArrBms(chessBoardAdapter!!, arrBms)
        chessBoardAdapter!!.notifyDataSetChanged()
        turnO = true
        txtTurn!!.text = "turn of O"
        imgStroke!!.setImageBitmap(null)
    }

    companion object {
        var turnO = true
        var txtTurn: TextView? = null
        var txtWinX: TextView? = null
        var txtWinO: TextView? = null
        var txtWin: TextView? = null
        var imgStroke: ImageView? = null
        var imgWin: ImageView? = null
        var rlWin: RelativeLayout? = null
        var TAG: String = GameFragment::class.java.name
    }
}