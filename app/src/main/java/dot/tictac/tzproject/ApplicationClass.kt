package dot.tictac.tzproject

import android.app.Application
import android.content.SharedPreferences
import com.onesignal.OneSignal

class MainApplication : Application() {

    private val PREFS = "prefs"
    lateinit var prefs: SharedPreferences

    override fun onCreate() {
        super.onCreate()

        prefs = getSharedPreferences(PREFS, MODE_PRIVATE)

        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)

        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }
}