package dot.tictac.tzproject

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import dot.tictac.tzproject.fragments.StartFragment
import dot.tictac.tzproject.fragments.WebFragment

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        MainApplication()
        setContentView(R.layout.activity_main)

        val transaction = supportFragmentManager.beginTransaction()
        val networkConnection = NetworkConnection(applicationContext)

        if (networkConnection.isInternetConnected) {
            transaction.replace(R.id.main_frame, WebFragment())
            transaction.commit()
        } else {
            transaction.replace(R.id.main_frame, StartFragment())
            transaction.commit()
        }
    }

    companion object {
        var scoreX = 0
        var scoreO = 0
    }
}