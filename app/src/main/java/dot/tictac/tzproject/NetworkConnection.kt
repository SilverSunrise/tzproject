package dot.tictac.tzproject

import android.content.Context
import android.net.ConnectivityManager


@Suppress("DEPRECATION")
class NetworkConnection(private val mContext: Context) {
    private var isConnected = false
    private var mConnectivityManager: ConnectivityManager? = null
    val isInternetConnected: Boolean
        get() {

            mConnectivityManager =
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val mNetworkInfo = mConnectivityManager!!.activeNetworkInfo
            isConnected =
                mNetworkInfo != null && mNetworkInfo.isAvailable && mNetworkInfo.isConnected
            return isConnected

        }

}